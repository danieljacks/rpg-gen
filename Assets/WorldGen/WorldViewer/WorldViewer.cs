﻿using UnityEngine;
using System.Collections;
using WGen;
using WGen.Noise;

[RequireComponent(typeof(SpriteRenderer))]
public class WorldViewer : Singleton<WorldViewer> {
	protected WorldViewer () {}

	//Fields
	[SerializeField]
	private int _viewTexSize = 512;
	[SerializeField]
	private int _viewSize = World.SizeTiles;
	public float _baseFrequency = 10f;
	private Texture2D _viewTexture;
	private Coroutine _updateRoutine;
	private SpriteRenderer _spriteRenderer;
	private SpriteRenderer _spriteRenderer2;

	public Vector2 TEMPOffset = Vector2.zero;
	public float zoom = 1f;
	public Vector2 repeat = new Vector2 (1, 1);
	public Vector2 viewPosPercent = Vector2.zero;
	public float TEMPPercentShown = 1f;
	public float TEMPFrequency = 0.005f;

	//private Vector2 _viewPos;

	//Properties
	///What the view currently displays. Changes auatomatically updates view
	public Texture2D ViewTexture {
		get {
			if (_viewTexture == null) {
				return _viewTexture = new Texture2D ((int) _viewSize, (int) _viewSize);
			} else {
				return _viewTexture;
			}
		}
		set {
			_viewTexture = value;
			UpdateView ();
		}
	}

	///Position of top left corner of view in tile coordinates
	public Vector2 ViewPos {get; set;}

	//Size in of view in Pixels
	public int ViewTexSize {
		get {
			return _viewTexSize;
		}
		set {
			_viewTexSize = value;
			UpdateView ();
		}
	}

	///Size of view in Tiles
	public int ViewSize {
		get {
			return _viewSize;
		}
		set {
			_viewSize = value;
			UpdateView ();
		}
	}

	public float ViewTilesPerPixel {
		get {
			return ViewSize / ViewTexSize; // total tiles / total pixel
		}
	}

	///Reference to the WorldViewer sprite renderer
	public SpriteRenderer SPRenderer {
		get {
			if (_spriteRenderer == null) {
				_spriteRenderer = GetComponent<SpriteRenderer>();
			}
			return _spriteRenderer;
		}
	}

	///Duplicate display of SPRenderer. Used to check tiling
	public SpriteRenderer SPRenderer2 {
		get {
			if (_spriteRenderer2 == null) {
				 GameObject GO = new GameObject("SpriteRenderer2");
				 //GO.transform.parent = transform;
				 GO.transform.position = new Vector3 (0, -3, 0); //No idea about these coordinates
				_spriteRenderer2 = GO.AddComponent<SpriteRenderer> ();
			}
			return _spriteRenderer2;
		}
	}

	void Start () {
		UpdateView ();
	}

	///Call to update display. Ensures update is called no more than once per frame
	public void UpdateView () {
		//Only one update per frame
		if (_updateRoutine == null) {
			_updateRoutine = StartCoroutine (UpdateViewRoutine ());
		}

	}

	//Coroutine allows WaitForEndOfFrame, so multiple view updates are never called in one frame. Dont call directly, call UpdateView () instead.
	private IEnumerator UpdateViewRoutine () {

		yield return new WaitForEndOfFrame ();

		//NoiseParameters parameters = GenerateParameters (zoom, viewPosPercent, NoiseParameters.DefaultPerlin.Frequency);
		NoiseParameters parameters = GenerateParameters (zoom, viewPosPercent, _baseFrequency);

		//Generate
		Texture2D vTex = GenerateTerrain.GenerateTexture (ViewTexSize, parameters);

		//DisplayTexture
		SPRenderer.sprite = Sprite.Create (vTex, new Rect (0, 0, ViewTexSize, ViewTexSize), Vector2.zero); 

		//FOR 2 TILES
		//parameters.offset += ViewPos;
		vTex = GenerateTerrain.GenerateTexture (ViewTexSize, parameters);
		SPRenderer2.sprite = Sprite.Create (vTex, new Rect (0, 0, ViewTexSize, ViewTexSize), new Vector2 (-1, 0)); 

		_updateRoutine = null; //Allows coroutine to be called next frame
	}

	private NoiseParameters GenerateParameters (float zoom, Vector2 viewPos, float fullFrequency) { //Where viewPos is a percentage of x and y i.e. 0..1 range
		//Init
		//Keep coordinates as percentages of initial frequency/size
		//Vector2 posFromTopLeft = new Vector2 (viewPos.x, 1 - viewPos.y);
		//Vector2 viewDimensions = new Vector2 (TEMPPercentShowns, TEMPPercentShowns);

		//float noiseFrequency = TEMPPercentShowns * fullFrequency;
		//Vector2 noiseOffset = new Vector2 (posFromTopLeft.x, posFromTopLeft.y - (viewDimensions.y));
		//print ("RAW: " + ( noiseOffset + (TEMPOffset * fullFrequency)).ToString () );
		
		//noiseOffset /= noiseFrequency;
		//noiseOffset = Vector2.zero;
		//noiseOffset += TEMPOffset;
		//noiseFrequency = TEMPFrequency;

		//noiseOffset = TEMPOffset;
		//print ("offset: " + noiseOffset);
		float displayPercent = 1 / zoom;
		float viewSize = displayPercent * fullFrequency; //view frequency
		print ("viewSize: " + viewSize);

		float noiseFrequency = fullFrequency / zoom;

		Vector2 noiseOffset = new Vector2 (viewPos.x * fullFrequency, (viewPos.y * fullFrequency + viewSize) - fullFrequency ); // / viewSize; //fullFrequency - (viewPos.y * fullFrequency) );   //1 - //(viewPos.y + viewSize) ); // /viewSize; //Thats a divided sign. Not a typo.
		//noiseOffset *= fullFrequency;
		noiseOffset = new Vector2 (viewPos.x * zoom, (-viewPos.y * zoom + zoom) - 1);
		//noiseOffset = Vector2.zero;
		//Vector2 noiseOffset = new Vector2 (0, -noiseFrequency);
		print ("Full: " + fullFrequency + " | viewPos: " + viewPos +" | Offset: " + noiseOffset);
		noiseOffset += TEMPOffset; //TEMP--------------------

		//Parameters
		NoiseParameters parameters = NoiseParameters.DefaultPerlin;
		parameters.Frequency = noiseFrequency;
		parameters.Offset = noiseOffset;
		parameters.Repeat = repeat;

		return parameters;
	}

	///Return (rounded) tile position at the normal position on view (0..1)
	public Vector2 TileCoordinates (Vector2 viewNormalPos) {
		//Tile pos 	= (Tiles per pixel * number of pixels at view pos from corner) + pixel pos view corner
		//			= (Tiles per pixel * (number of pixels in view * view normal pos (0..1))) + pixel pos view corner
		Vector2 tilePos = new Vector2 (ViewTilesPerPixel * (ViewTexSize * viewNormalPos.x) + ViewPos.x , ViewTilesPerPixel * (ViewTexSize * viewNormalPos.y) + ViewPos.y );
		tilePos.x = (int) tilePos.x;
		tilePos.y = (int) tilePos.y;

		return tilePos;
	}

}
