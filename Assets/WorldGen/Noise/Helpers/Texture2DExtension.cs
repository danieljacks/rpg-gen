﻿using UnityEngine;

public static class Texture2DExtension {

	///Print coordinates and values of up to 15 random pixels from the texture
	public static void PrintSamples (this Texture2D texture, int samples) {
		samples = Mathf.Min(samples, 15);

		Random.State startState = Random.state; //Cache seed so testing function does not change it (and mess something up that will be hard to debug)

		for (int i = 0; i < samples; i++) {
			int x = Random.Range(0, texture.width);
			int y = Random.Range(0, texture.width);
			Debug.Log ("X: " + y + " | Y: " + y + " | RGB: " + texture.GetPixel(x, y).ToString());
		}

		Random.state = startState;
	}

}
