﻿using UnityEngine;

public static class RenderTextureExtension {

	public static Texture2D ToTexture2D (this RenderTexture renderTex) {
		//Init
		Texture2D texture2D = new Texture2D (renderTex.width, renderTex.height);

		//Copy
		RenderTexture.active = renderTex;
		texture2D.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
		texture2D.Apply();

		//Cleanup
		RenderTexture.active = null;
		renderTex.Release();

		return texture2D;
	}
}
