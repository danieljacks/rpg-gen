﻿using UnityEngine;

namespace WGen.Noise {
public class Perlin {


	public NoiseParameters Parameters {get; set;}
	///Where in the world the perlin noise covers. 0 is the top-let tile.
	public Rect Area {get; set;}
	

	/*public NoiseParameters paremeters;
	public Material perlinMat;
	private static Material staticPerlinMat;

	void Start () {
		staticPerlinMat = perlinMat;
	}*/

	public Perlin (NoiseParameters parameters, Rect area) {
		Parameters = parameters;
		Area = area;
	}

	public Perlin (float baseFrequency, NoiseParameters parameters, Rect area) {
		parameters.Frequency = baseFrequency;
		Parameters = parameters;
		Area = area;
	}

	///Creates a square perlin noise using the gpu
	public static Texture2D Generate (int size, NoiseParameters parameters, bool tileable) {
		//Init
		/*perlinMat.SetFloat ("_Octaves", 8.0f);
		perlinMat.SetFloat ("_Frequency", 2.0f);
		perlinMat.SetFloat ("_Amplitude", 1.0f);
		perlinMat.SetFloat ("_Lacunarity", 1.92f);
		perlinMat.SetFloat ("_Persistence", 0.8f);*/

		Texture2D perlinTex = new Texture2D (size, size);
		Texture2D blankTex = new Texture2D (1,1); //Constant.Generate (size, new Color32 (255, 0, 0, 255));

		Material perlinMat;
		if (tileable) {
			perlinMat = NoiseShaders.Instance.PerlinTileableMat;
		} else {
			perlinMat = NoiseShaders.Instance.PerlinMat;
		}
		
		RenderTexture perlinRenderTex = NoiseShaders.Instance.PerlinRenderTexture (size);

		//Parameters
		perlinMat.SetFloat ("_Octaves", parameters.Octaves);
		perlinMat.SetFloat ("_Frequency", parameters.Frequency);
		perlinMat.SetFloat ("_Amplitude", parameters.Amplitude);
		perlinMat.SetFloat ("_Lacunarity", parameters.Lacunarity);
		perlinMat.SetFloat ("_Persistence", parameters.Persistence);
		perlinMat.SetVector ("_Offset", new Vector4 (parameters.Offset.x, parameters.Offset.y, 0, 0) );
		if (tileable) {
			perlinMat.SetVector ("_Repeat", new Vector4 (parameters.Repeat.x, parameters.Repeat.y, 0, 0) );
		}

		//Generate
		Graphics.Blit (blankTex, perlinRenderTex, perlinMat); //Copy perlin texture into render texture

		perlinTex = perlinRenderTex.ToTexture2D ();

		return perlinTex;
	}

	public Tilemap 



}
}
