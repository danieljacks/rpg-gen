﻿using UnityEngine;

namespace WGen.Noise {
///Reference for materials used to create noise on the gpu
public class NoiseShaders : Singleton<NoiseShaders> {

	protected NoiseShaders () {}

	public Shader perlin;
	public Shader perlinTileable;

	public Material tempPerlinMat;
	public Material tempPerlinTileableMat;

	public RenderTexture tempPerlinRenderTexture;

	public Material PerlinMat {
		get {
			//return new Material(NoiseShaders.Instance.perlin);
			return tempPerlinMat;
		}
	}

	public Material PerlinTileableMat {
		get {
			//return new Material(NoiseShaders.Instance.perlinTileable);
			return tempPerlinTileableMat;
		}
	}

	public RenderTexture PerlinRenderTexture (int size) {
		RenderTexture renderTex = new RenderTexture (size, size, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
		renderTex.wrapMode = TextureWrapMode.Clamp;
		renderTex.filterMode = FilterMode.Point;

		renderTex.Create ();
		return renderTex;

		//return tempPerlinRenderTexture;
	}

}
}