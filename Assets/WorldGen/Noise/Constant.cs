﻿using UnityEngine;

namespace WGen.Noise {
public class Constant {

	//Returns texture of size filled with color
	public static Texture2D Generate (int size, Color32 color) {
		Color32[] pixels = new Color32[size * size];
		for (int y = 0, i = 0; y < size; y++) {
			for (int x = 0; x < size; x++, i++) {
				pixels[i] = color;
			}
		}

		Texture2D blank = new Texture2D (size, size);

		blank.SetPixels32 (pixels);

		blank.Apply();
		return blank;
	}
	
}
}