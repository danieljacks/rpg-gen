﻿using UnityEngine;
using ProtoBuf;

[ProtoContract]
public class TextureProto {
	//Fields
	[ProtoMember(1)]
	private Color32[] _tiles1d;
	private Color32[,] _tiles;
	private const int _size = 16;

	//Properties
	public static int Width { 
		get { return _size; } 
	}
	public static int Height { 
		get { return _size; } 
	}

	public static int Size {
		get {return _size; }
	}

	public static TextureProto Empty {
		get {
			return CreateEmpty();
		}
	}

	//Constructors
	///Initialise empty chunk
	public TextureProto () {
		_tiles = new Color32[_size, _size];
	}

	public TextureProto (Color32[,] tileValues) : this () {
		if (tileValues.GetLength(0) == _size && tileValues.GetLength(1) == _size) {
			_tiles = (Color32[,])tileValues.Clone();
		} else {
			Debug.LogError("Given array does not match the dimensions of a chunk.");
		}
	}

	[ProtoBeforeSerializationAttribute]
	void Initialise1dTileArray () {
		_tiles1d = _tiles.Flatten();
	}

	[ProtoAfterSerializationAttribute]
	void Dispose1dTileArray () {
		_tiles1d = null;
	}

	[ProtoAfterDeserializationAttribute]
	void Initialise2dArray () {
		_tiles = _tiles1d.Expand(_size, _size);
		_tiles1d = null;
	}

	private static TextureProto CreateEmpty () {
		Color32[,] tiles = new Color32[_size, _size];

		for (int y = 0; y < _size; y++) {
			for (int x = 0; x < _size; x++) {
				tiles[x, y] = new Color32 (0, 0, 0, 0);
			}
		}

		return new TextureProto (tiles);
	}

	//Methods
	public Color32 this [int x, int y] {
		get {
			if (x < 0 || x >= _size)
			{
				throw new System.ArgumentOutOfRangeException("Invalid x position");
			}
			if (y < 0 && y >= _size)
			{
				throw new System.ArgumentOutOfRangeException("Invalid y position");
			}
			return _tiles[x, y];
		}
		set {
			if (x < 0 || x >= _size)
			{
				throw new System.ArgumentOutOfRangeException("Invalid x position");
			}
			if (y < 0 && y >= _size)
			{
				throw new System.ArgumentOutOfRangeException("Invalid y position");
			}
			_tiles[x, y] = value;
		}
	}

	private Color32 GetValue (int x, int y) {
		return _tiles[x, y];
	}

	public Tile GetTile (int x, int y) {
		//Tile tile = new Tile();

		//Color32 key = GetValue(x, y);
		//tile = Tile.GetTile(key);

		return new Tile();
	}

	public Texture2D GenerateTexture () {
		Texture2D tex = new Texture2D(_size, _size);
		Color32[] pixels = new Color32[_size * _size];

		for (int y = 0, i = 0; y < _size; y ++) {
			for (int x = 0; x < _size; x++, i++) {
				pixels[i] = _tiles[x, y];
			}
		}

		tex.SetPixels32(pixels);
		tex.Apply();

		return tex;
	}
}
