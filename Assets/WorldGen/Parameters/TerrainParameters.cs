﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainParameters : MonoBehaviour {

	[RangeAttribute(0f, 1.0f)]
	public float waterLevel;
	[RangeAttribute(0f, 1.0f)]
	/// <summary>
    /// Distance sand reaches above water where 1 is the heightmap full height
    /// </summary>
	public float sandDistance;

	///Return default terrain parameter instance
	public static TerrainParameters Default {
		get {
			TerrainParameters p = new TerrainParameters ();
			p.SetDefaults ();
			return p;
		}
	}

	///Sets default values for this instance
	public void SetDefaults () {
		
	}

	public TerrainParameters () {
		
	}
}
