﻿using UnityEngine;
using System.Collections;

namespace WGen.Noise {
public class NoiseParameters {
	//MAKE PARAMETERS [PRIVATE] AND HAVE FIELDS TO ACCESS THEM THAT CAN RETURN DEFAULT VALUES IF NOTHING SET, OR SET DEFAULT VALUES BY VARIABLE DECLARATION (IF THAT WORKS)
	private int _seed = 71;
	public int Seed {get {return _seed; } private set {_seed = value;}}
	public int Octaves {get; set;}
	public float Frequency {get; set;}
	public float Amplitude {get; set;}
	public float Lacunarity {get; set;}
	public float Persistence {get; set;}
	public Vector2 Offset {get; set;}
	public Vector2 Repeat {get; set;}

	///Returns an instance of NoiseParameters with all values set to their defaults
	public static NoiseParameters DefaultPerlin {
		get {
			return new NoiseParameters ();
		}
	}

	///A collection of all default NoiseParameters values
	private static class Defaults {
		//Defaults
		public static int Octaves { get {return 6;}} //REAL - 6
		public static float Frequency { get {return 10f;}} //0.005f; OLD BEFORE SHADER USED UV COORDS //2.0f -values on shader
		public static float Amplitude { get{return 1.0f;}} //?
		public static float Lacunarity { get{return 2.1f;}} //1.92
		public static float Persistence { get{return 0.62f;}} //0.8f
		public static Vector2 Offset { get{return new Vector2 (0, 0);}}
		public static Vector2 Repeat { get{return new Vector2 (1, 1);}}
	}

	public NoiseParameters () {
		Offset = Defaults.Offset;
		Frequency = Defaults.Frequency;
		Amplitude = Defaults.Amplitude;
		Lacunarity = Defaults.Lacunarity;
		Persistence = Defaults.Persistence;
		Octaves = Defaults.Octaves;
		Repeat = Defaults.Repeat;
	}

	public NoiseParameters (Vector2 _offset, float _frequency) {
		Offset = _offset;
		Frequency = _frequency;
		Amplitude = Defaults.Amplitude;
		Lacunarity = Defaults.Lacunarity;
		Persistence = Defaults.Persistence;
		Octaves = Defaults.Octaves;
		Repeat = Defaults.Repeat;
	}

	public NoiseParameters (Vector2 _offset, float _frequency, float _amplitude, float _lacunarity, float _persistence, int _octaves, Vector2 _repeat) {
		this.Octaves = _octaves;
		this.Frequency = _frequency;
		this.Amplitude = _amplitude;
		this.Lacunarity = _lacunarity;
		this.Persistence = _persistence;
		this.Offset = _offset;
		this.Repeat = _repeat;
	}

	///Converts and sets seed into acceptable range (71 - 270)
	public void SetSeed (int seed) {
		//Linear Congruential Generator
		int a = 1103515245; 
		int c = 12345;

		Vector2 vSeed = GetSeedVector (seed);
		vSeed.x = (vSeed.x * a + c) % 300 - 150;
		vSeed.y = (vSeed.y * a + c) % 300 - 150;

		string seedString = vSeed.x.ToString () + vSeed.y.ToString ();

		Seed = int.Parse (seedString);

		Debug.Log ("Set seed to: " + int.Parse (seedString));
	}

	///Get seed x and y from single seed value by splitting seed number randomly into 2 parts (x and y components)
	public Vector2 GetSeedVector () {
		return GetSeedVector (Seed);
	}

	private Vector2 GetSeedVector (int seed) {
		string sd = seed.ToString();

		int x = int.Parse ( sd.Substring(0, (int) sd.Length / 2) );
		int y = int.Parse ( sd.Substring((int) sd.Length / 2, sd.Length) );

		return new Vector2 (x, y);
	}

	/*public static NoiseParameters GenerateParameters (float zoom, Vector2 viewPos, float fullFrequency) { //Where viewPos is a percentage of x and y i.e. 0..1 range
		//Init
		//Keep coordinates as percentages of initial frequency/size
		//Vector2 posFromTopLeft = new Vector2 (viewPos.x, 1 - viewPos.y);
		//Vector2 viewDimensions = new Vector2 (TEMPPercentShowns, TEMPPercentShowns);

		//float noiseFrequency = TEMPPercentShowns * fullFrequency;
		//Vector2 noiseOffset = new Vector2 (posFromTopLeft.x, posFromTopLeft.y - (viewDimensions.y));
		//print ("RAW: " + ( noiseOffset + (TEMPOffset * fullFrequency)).ToString () );
		
		//noiseOffset /= noiseFrequency;
		//noiseOffset = Vector2.zero;
		//noiseOffset += TEMPOffset;
		//noiseFrequency = TEMPFrequency;

		//noiseOffset = TEMPOffset;
		//print ("offset: " + noiseOffset);
		float displayPercent = 1 / zoom;
		float viewSize = displayPercent * fullFrequency;

		float noiseFrequency = fullFrequency / zoom;

		Vector2 noiseOffset = new Vector2 (viewPos.x * fullFrequency , 1 - (viewPos.y + viewSize) ) / viewSize;
		noiseOffset += TEMPOffset; //TEMP--------------------

		//Parameters
		NoiseParameters parameters = NoiseParameters.DefaultPerlin;
		parameters.frequency = noiseFrequency;
		parameters.offset = noiseOffset;
		parameters.repeat = repeat;

		return parameters;
	}*/

}
}