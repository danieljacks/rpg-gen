﻿using UnityEngine;

namespace WGen {
public class Tilemap {

	//Properties
	public int Width {get {return _width;}}
	public int Height {get {return _height;}}

	//Fields
	private byte[,] _tilemap;
	private int _width;
	private int _height;
	private WGenTerrain _gen;

	//Constructor
	public Tilemap (int width, int height) {
		this._width = width;
		this._height = height;
		this._tilemap = new byte[width, height];
	}
	///Get or set value in array
	public byte this [int x, int y] {
		get {
			if (x < 0 || x >= _width)
			{
				throw new System.ArgumentOutOfRangeException("Invalid x position");
			}
			if (y < 0 && y >= _height)
			{
				throw new System.ArgumentOutOfRangeException("Invalid y position");
			}
			return _tilemap[x, y];
		}
		set {
			if (x < 0 || x >= _width)
			{
				throw new System.ArgumentOutOfRangeException("Invalid x position");
			}
			if (y < 0 && y >= _height)
			{
				throw new System.ArgumentOutOfRangeException("Invalid y position");
			}
			_tilemap[x, y] = value;
		}
	}

	private byte GetValue (int x, int y) {
		return _tilemap[x, y];
	}

	public Tile GetTile (int x, int y) {
		Tile tile = new Tile();

		byte key = GetValue(x, y);
		tile = Tile.GetTile(key);

		return tile;
	}

	public Texture2D GenerateTexture () {
		Texture2D tex = new Texture2D(_width, _height);
		Color32[] pixels = new Color32[_width * _height];

		for (int y = 0, i = 0; y < _height; y ++) {
			for (int x = 0; x < _width; x++, i++) {
				pixels[i] = GetTile(x, y).color;
			}
		}

		tex.SetPixels32(pixels);
		tex.Apply();

		return tex;
	}
}
}