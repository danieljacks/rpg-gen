﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
public class TerrainRenderer : Singleton<TerrainRenderer> { //Make this singleton? Make editor for wgenterrain that calls texture drawer on change
	protected TerrainRenderer () {} //Cant create class with constructor
	public bool change;

	private Texture2D texture;
	private SpriteRenderer spriteRenderer;
	private Texture2D combinedChunkTex;
	public GameObject childSprite;
	private Dictionary<Vector2, SpriteRenderer> spriteDictionary;

	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer>();

		//DrawTerrain();

		combinedChunkTex = new Texture2D (World.SizeTiles, World.SizeTiles); 

		spriteDictionary = new Dictionary<Vector2, SpriteRenderer>();
	}

	void Update () {
		
	}

	public void DrawTerrain () {
		//DrawTerrainOld();
		//DrawNoiseLib();
		DrawChunkDictionary();
	}

	void OnGUI () {
		Vector2 pos = new Vector2 (0, 0);
		Texture2D drawTex;
		SpriteRenderer spRenderer;

		pos.x = 0;
		pos.y = 0;

		if (WGenTerrain.Instance.terrainTexDictionary.TryGetValue(pos, out drawTex)) {
			if (!spriteDictionary.TryGetValue(pos, out spRenderer)) {
				GameObject obj = (GameObject) Instantiate (childSprite, pos, Quaternion.identity);
				spRenderer = obj.GetComponent<SpriteRenderer>();
				spriteDictionary.Add(pos, spRenderer);
			}

			GUI.DrawTexture (new Rect (0, 0, drawTex.width, drawTex.height), drawTex );
		}
	}

	public void DrawChunkDictionary () {
		Vector2 pos = new Vector2 (0, 0);
		Texture2D drawTex;
		SpriteRenderer spRenderer;

		for (int x = 0; x < World.SizeChunks; x++) {
			for (int y = 0; y < World.SizeChunks; y++) {
				pos.x = x;
				pos.y = y;

				if (WGenTerrain.Instance.terrainTexDictionary.TryGetValue(pos, out drawTex)) {
					if (!spriteDictionary.TryGetValue(pos, out spRenderer)) {
						GameObject obj = (GameObject) Instantiate (childSprite, pos, Quaternion.identity);
						spRenderer = obj.GetComponent<SpriteRenderer>();
						spriteDictionary.Add(pos, spRenderer);
					}

					spRenderer.sprite = Sprite.Create (drawTex, new Rect (0, 0, drawTex.width, drawTex.height), new Vector2 (0.5f, 0.5f));
				}
			}
		}
	}

	private void DrawNoiseLib () {
		if (WGenTerrain.Instance.terrainTexture == null) {
			print ("Could not find WGen texture, skipping chunk render...");
			return;
		}

		Texture2D spriteTex = WGenTerrain.Instance.terrainTexture;

		int tSize = WGenTerrain.Instance.terrainTexture.width;

		spriteRenderer.sprite = spriteRenderer.sprite = Sprite.Create(spriteTex, new Rect(0, 0, tSize, tSize), new Vector2(0.5f, 0.5f));
	}

	private void DrawTerrainOld () {
		byte[,] terrain = WGenTerrain.Instance.terrain;

		int tWidth = terrain.GetLength(0);
		int tHeight = terrain.GetLength(1);

		//print ("twidth: " + tWidth);

		//for (int i = 0; i < 15; i++) {
		//	print (terrain[Random.Range(0,512), Random.Range(0,512)]);
		//}

		Color32[] flatTex = new Color32[terrain.Length];
		
		for (int y = 0, i = 0; y < tWidth; y++) {
			for (int x = 0; x < tHeight; x++, i++) {
				byte v = terrain[x,y];
				flatTex[i] = new Color32(v, v, v, 255);
			}
		}

		Texture2D spriteTex = new Texture2D(tWidth, tHeight);
		spriteTex.SetPixels32(flatTex);
		spriteTex.Apply();

		spriteRenderer.sprite = Sprite.Create(spriteTex, new Rect(0, 0, tWidth, tHeight), new Vector2(0.5f, 0.5f));
	}
}