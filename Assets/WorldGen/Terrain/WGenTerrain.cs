﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using LibNoise;
using LibNoise.Generator;
using LibNoise.Operator;

using WGen;


/// <summary>
/// Sandbox for terrain generation
/// </summary>
public class WGenTerrain : Singleton<WGenTerrain> {
	protected WGenTerrain () {} //Can't use constructor for this class
	public bool useGpuRendering = true;
	public RenderTexture perlinRenderTex;
	public Material perlinMat;
	private Texture2D blankTex;
	public bool simplex = false;
	public int seed = 42;
	public int terrainSize = 512;
	public Vector2 perlinScale = new Vector2(10,10);
	[System.NonSerializedAttribute]
	public byte[,] terrain;

	public Texture2D terrainTexture;
	public Dictionary<Vector2, Texture2D> terrainTexDictionary;

	public float frequency = 0.012f;
	public float lacunarity = 1.6f;
	public float roughness = 0.6f;
	public int octaves = 6;
	[SpaceAttribute(5)]
	[HeaderAttribute("Tile Values")]
	[RangeAttribute(0f, 1.0f)]
	public float waterLevel;
	[RangeAttribute(0f, 1.0f)]
	/// <summary>
    /// Distance sand reaches above water where 1 is the heightmap full height
    /// </summary>
	public float sandDistance;



	//GUI stuff
	private string xLoad = "0";
	private string yLoad = "0";

	void Start () {
		terrain = new byte[terrainSize, terrainSize];
		blankTex = GenerateBlank(terrainSize);
		terrainTexture = new Texture2D (16, 16);
		terrainTexDictionary = new Dictionary<Vector2, Texture2D>();
		//Generate ();
		print ("Sectors will be saved to: " + World.SectorDirectory);
	}

	void OnGUI () {
		Rect buttonPos = new Rect (50, 50, 100, 30);

		if (GUI.Button(buttonPos, "Generate")) {
			Generate();
		}
		buttonPos.y += 50;

		int r;
		Vector2 chunkLoadPos = Vector2.zero;

		Rect bPosTemp = buttonPos;

		buttonPos.y += 50;
		xLoad = GUI.TextField (buttonPos, xLoad);

		if (int.TryParse(xLoad, out r)) {
			chunkLoadPos.x = r;
		}
		buttonPos.y += 50;

		yLoad = GUI.TextField (buttonPos, yLoad);

		if (int.TryParse(yLoad, out r)) {
			chunkLoadPos.y = r;
		}

		buttonPos.y += 50;
		if (GUI.Button(buttonPos, "RenderChunk")) {
			TerrainRenderer.Instance.DrawTerrain();
		}

		buttonPos.y += 50;
		if (GUI.Button(buttonPos, "TestArray")) {
			print ("flat val: " + ArrayHelpers.IndexGridToFlat (new Vector2 (0, 0), 16, true));
		}

		buttonPos = bPosTemp;
		if (GUI.Button(buttonPos, "LoadChunk")) {
			//RenderChunk (chunkLoadPos);
			StopCoroutine(LoadChunksToDictionary());
			StartCoroutine(LoadChunksToDictionary());
		}
	}

	void Generate () {
		/*Noise2D perlin = GeneratePerlin0(terrainSize);

		Tilemap tilemap = AssignTiles0(perlin);

		terrainTexture = tilemap.GenerateTexture();*/

		StartCoroutine(GenerateTerrainWithSavingTexture());
	}

	public void Regenerate () {
		//GenerateLand(terrain, seed);

		//terrainTexture = GeneratePerlin(terrainSize);

		Generate();
	}

	Texture2D GenerateBlank (int size) {
		Color32[] pixels = new Color32[size * size];
		for (int y = 0, i = 0; y < size; y++) {
			for (int x = 0; x < size; x++, i++) {
				pixels[i] = new Color32 (0, 0, 0, 0);
			}
		}

		Texture2D blank = new Texture2D (size, size);

		blank.SetPixels32 (pixels);

		blank.Apply();
		return blank;
	}

	private Noise2D GeneratePerlin0 (int size) {
		return GeneratePerlin0 (size, Vector2.zero);
	}

	private Noise2D GeneratePerlin0 (int size, Vector2 offset) {
		Perlin perlin = new Perlin(frequency, lacunarity, roughness, octaves, seed, QualityMode.High);
		ModuleBase module = perlin;
		
		Noise2D heightMap = new Noise2D(size, size, module);
		heightMap.GeneratePlanar(offset.x, offset.x + size, offset.y, offset.y + size, false);

		return heightMap;
	}

	private void GenerateLandO (byte[,] terrain, int seed) {
		float offset = seed;

		int width = terrain.GetLength(0);
		int height = terrain.GetLength(1);

		byte[,] perlin = new byte[width, height];

		for (int y = 0; y < height; y++) { //Create perlin stuff
			for (int x = 0; x < width; x++) {
				float noise255;
				if (simplex) {
					noise255 = SimplexNoise.SeamlessNoise((float)x / (float)width, (float)y / (float)height, perlinScale.x, perlinScale.y, offset);
					noise255 = (noise255 + 1.0f) * 0.5f; //0-1 range
				} else {
					noise255 = Mathf.PerlinNoise(( (float)x * perlinScale.x) / (float)width, ( (float)y * perlinScale.y ) / (float)height);
				}
				//print ("w: " + ((float)x / (float)width));
				//noise255 = (noise255 + 1.0f) * 0.5f; //0-1 range
				noise255 *= 255f; //0-255 range
				perlin[x, y] = (byte)noise255;
			}
		}

		for (int y = 0; y < height; y++) { //Copy to original array
			for (int x = 0; x < width; x++) {
				terrain[x, y] = perlin[x, y];
			}
		}
	}

	private Chunk AssignTiles (Noise2D heightmap) {
		Chunk chunk = new Chunk();

		for (int y = 0; y < Chunk.SizeTiles; y++) {
			for (int x = 0; x < Chunk.SizeTiles; x++) {
				chunk[x, y] = GenerateTile(heightmap[x, y]);
			}
		}

		return chunk;
	}

	private Chunk AssignTiles (Texture2D heightmap) {
		Chunk chunk = new Chunk();

		for (int y = 0; y < Chunk.SizeTiles; y++) {
			for (int x = 0; x < Chunk.SizeTiles; x++) {
				chunk[x, y] = GenerateTile(heightmap.GetPixel(x, y).grayscale);
			}
		}

		return chunk;
	}

	///Generate tile id based on heightmap (0..1) float input
	private byte GenerateTile (float height) {
		//height = ( height + 1 ) * 0.5f; //Normalize height
		byte tile = 1;

		if (height <= waterLevel) {
			tile = 0;
		} else if (height <= waterLevel + sandDistance) {
			tile = 2;
		} else if (height < waterLevel + sandDistance + 0.1f) { //EXTRA FOR TESTING
			tile = 1;
		} else if (height < waterLevel + sandDistance + 0.2f) { //EXTRA FOR TESTING
			tile = 3;
		} else if (height < waterLevel + sandDistance + 0.3f) { //EXTRA FOR TESTING
			tile = 4;
		} else if (height < waterLevel + sandDistance + 0.4f) { //EXTRA FOR TESTING
			tile = 5;
		} else if (height < waterLevel + sandDistance + 0.5f) { //EXTRA FOR TESTING
			tile = 6;
		} else if (height < waterLevel + sandDistance + 0.6f) { //EXTRA FOR TESTING
			tile = 7;
		} 

		return tile;
	}

	private Noise2D GeneratePerlin (int size) { 
		Perlin perlin = new Perlin(frequency, lacunarity, roughness, octaves, seed, QualityMode.High);
		ModuleBase module = perlin;
		
		Noise2D heightMap = new Noise2D(size, size, module);
		return heightMap;
	}

	IEnumerator GenerateTerrainWithSaving () {
		Noise2D worldPerlin = GeneratePerlin (terrainSize);
		
		int wSize = World.SizeChunks;
		print ("World dimensions in chunks:" + wSize);

		Chunk chunk = new Chunk();

		for (int y = 0; y < wSize; y++) {
			for (int x = 0; x < wSize; x++) {
				chunk = GenerateChunk(worldPerlin, new Vector2 (x, y) );
				yield return null;
				string log = string.Format ("Generated chunk at: ({0},{1})", x, y);
				print (log);
				yield return null;
				World.SaveChunk(new Vector2 (x, y), chunk);
				yield return null;
				log = string.Format	("Saved chunk at:    ({0},{1})", x, y);
				print (log);
				yield return null;
			}
		}
	}

	IEnumerator GenerateTerrainWithSavingTexture () {
		Noise2D worldPerlin = GeneratePerlin (terrainSize);
		
		int wSize = World.SizeChunks;
		print ("World dimensions in chunks:" + wSize);

		ProtoTexture texture = new ProtoTexture();

		for (int y = 0; y < wSize; y++) {
			for (int x = 0; x < wSize; x++) {
				texture = GenerateChunkTexture (worldPerlin, new Vector2 (x, y));
				yield return null;
				string log = string.Format ("Generated chunk at: ({0},{1})", x, y);
				print (log);
				yield return null;
				World.SaveTexture(new Vector2 (x, y), texture);
				yield return null;
				log = string.Format	("Saved chunk at:    ({0},{1})", x, y);
				print (log);
				yield return null;
			}
		}
	}

	private Chunk GenerateChunk (Noise2D worldPerlin, Vector2 offset) {
		Noise2D chunkHeightmap = worldPerlin;
		Chunk chunk = new Chunk();

		if (useGpuRendering) {
			Texture2D perlinTex = new Texture2D(16, 16);
			Graphics.Blit (blankTex, perlinRenderTex, perlinMat);

			perlinRenderTex.Create(); //LOOK UP HOW TO USE RENDER TEXXTURES
			RenderTexture.active = perlinRenderTex; //Set render texture as texture to GetPixels from

			Graphics.Blit (blankTex, perlinRenderTex, perlinMat); //Copy perlin texture into render texture
			perlinTex.ReadPixels (new Rect (0, 0, perlinRenderTex.width, perlinRenderTex.height), 0, 0); //Actually read pixels from RenderTexture

			RenderTexture.active = null;
			perlinRenderTex.Release();

			chunk = AssignTiles (perlinTex); ///ASSIGN TILES
		} else {
			chunkHeightmap.GeneratePlanar(offset.x, offset.x + Chunk.SizeTiles, offset.y, offset.y + Chunk.SizeTiles, true); //CPU RENDERING
			print ("Generateing chunk with size of: " + Chunk.SizeTiles + " tiles.");
			chunk = AssignTiles (chunkHeightmap);
		}

		//return chunk;
		return chunk;
	}

	private ProtoTexture GenerateChunkTexture (Noise2D worldPerlin, Vector2 offset) {
		Noise2D chunkHeightmap = worldPerlin;
		Texture2D perlinTex = new Texture2D(ProtoTexture.SizeTiles, ProtoTexture.SizeTiles);

		if (useGpuRendering) {
			Graphics.Blit (blankTex, perlinRenderTex, perlinMat);

			perlinRenderTex.Create();
			RenderTexture.active = perlinRenderTex; //Set render texture as texture to GetPixels from

			Graphics.Blit (blankTex, perlinRenderTex, perlinMat); //Copy perlin texture into render texture
			perlinTex.ReadPixels (new Rect (0, 0, perlinRenderTex.width, perlinRenderTex.height), 0, 0); //Actually read pixels from RenderTexture

			RenderTexture.active = null;
			perlinRenderTex.Release();
		} else {
			chunkHeightmap.GeneratePlanar(offset.x, offset.x + Chunk.SizeTiles, offset.y, offset.y + Chunk.SizeTiles, true); //CPU RENDERING
			print ("Generateing chunk with size of: " + Chunk.SizeTiles + " tiles.");
			perlinTex = chunkHeightmap.GetTexture();

		}

		print ("Generated prototexture, original texture at 1,1 is: " + perlinTex.GetPixel(1,1));

		ProtoTexture tex = new ProtoTexture (perlinTex.GetPixels32());
		return tex;
	}

	IEnumerator LoadChunksToDictionary () {
		Vector2 pos = new Vector2 (0, 0);
		Texture2D outTex;

		int wSize = World.SizeChunks;
		for (int y = 0; y < wSize; y++) {
			for (int x = 0; x < wSize; x++) {
				pos.x = x;
				pos.y = y;

				Texture2D genTerrain = World.LoadTexture(pos).GenerateTexture();
				if (!terrainTexDictionary.TryGetValue(pos, out outTex)) {
					terrainTexDictionary.Add (pos, genTerrain);
				} else {
					terrainTexDictionary[pos] = genTerrain;
				}
				
				yield return null;
				print ("Loading chunk (" + x + "," + y + ")");
			}
		}
		print ("Finished loading chunks");

		terrainTexDictionary.TryGetValue(new Vector2 (0, 0), out outTex);
		print ("chunk tex 0,0 : " + outTex.GetPixel(1,1));

	}

	void RenderChunk (Vector2 chunkPos) {
		terrainTexture = World.LoadChunk (chunkPos).GenerateTexture();
	}

	//WRITE METHODS TO SAVE/LOAD TEXTURES TO FILE SO I CAN SEE WHATS HAPPENING WITH THE TERRAIN MAP, BOTH FOR DEBUGGING AND FUTURE REFERENCE
	//WRITE GENERIC METHODS
	//THEN I CAN SAVE ANY TYPE OF DATA EG ENTITY DATA
	//CREATE ABSTRACT BASE CLASS FOR DATA CONTAINERS AND MAKE DATA AS 1D ARRAY FOR SPEED AND EASY SAVING, MAKE PROPERTIES ACCESS AS 2D ARRAY

}
