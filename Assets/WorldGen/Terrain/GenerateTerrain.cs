﻿using UnityEngine;
using WGen.Noise;

/*
                                 Goals:
    -Generate basic terrain (perlin noise)
        * Texture2D
        * Chunk data
    -Preview terrain (Different class)
    -Generate specific part of terrain in detail (zoomed up to the tile level)

*/


/// <summary>
/// Generates terrain - Basically land features
/// </summary>
namespace WGen {
public class GenerateTerrain : Singleton<GenerateTerrain> {
    protected GenerateTerrain () {} //Can't use constructor for this class
	public RenderTexture perlinRenderTex;
	public Material perlinMat;
	private Texture2D blankTex;
	public int seed = 42;
	public int terrainSize = 512;
	public Vector2 perlinScale = new Vector2(10,10);
	[System.NonSerializedAttribute]
	public byte[,] terrain;
	public Texture2D terrainTexture;
	public float frequency = 0.012f;
	public float lacunarity = 1.6f;
	public float roughness = 0.6f;
	public int octaves = 6;
	[SpaceAttribute(5)]
	[HeaderAttribute("Tile Values")]
	[RangeAttribute(0f, 1.0f)]
	public float waterLevel;
	[RangeAttribute(0f, 1.0f)]
	/// <summary>
    /// Distance sand reaches above water where 1 is the heightmap full height
    /// </summary>
	public float sandDistance;


	private Texture2D TEMPmat;



	//GUI stuff
	private string xLoad = "0";
	private string yLoad = "0";

	void Start () {
		//GenerateTexture (NoiseParameters.DefaultPerlin);
	}

	public static Texture2D GenerateTexture (int size, NoiseParameters perlinParameters) {
		Texture2D perlinTex = Perlin.Generate(size, perlinParameters, true); //Create perlin texture
		return perlinTex;

		//GetComponent<SpriteRenderer>().sprite = Sprite.Create(perlinTex, new Rect (0, 0, perlinTex.width, perlinTex.height), new Vector2 (0, 0)); //Set to perlin texture
	}

	//MAKE A GenerateTextureArea (int minCoord, int maxCoord) OVERLOAD
	///Returns terrain texture from specified area
	//public static Texture2D GenerateTextureArea (float pos, float zoom) {

	//}

	//FILL THIS IN LATER
	void GenerateChunk () {

	}

	void Testing () {

	}

	public void GenerateTiles () {
		Perlin pContinents = new Perlin (5, )
	}

}
}