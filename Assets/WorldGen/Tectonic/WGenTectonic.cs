﻿using UnityEngine;
using System.Collections;

public class WGenTectonic : MonoBehaviour {
/*
	public int numberOfPoints = 10;
	public float radius = 3;
	public float distortStrength = 2;
	public float seed = 42;
	public float moveSpeed = 1f;
	private Vector2[] points;

	void Start () {
		points = new Vector2[numberOfPoints];

		points = CreatePointCircle(numberOfPoints, radius);
		
		Vector2[] pointsTarget = points;
		pointsTarget = DistortPoints(pointsTarget, distortStrength);

		StartCoroutine(movePointsToTarget(points, pointsTarget, moveSpeed));
	}

	IEnumerator movePointsToTarget (Vector2[] points, Vector2[] targets, float speed) {
		Vector2[] p = points;

		bool finished = true;
		do {
			finished = true;

			for (int i = 0; i < p.Length; i++) {
				Vector2 moveTo = Vector2.MoveTowards(p[i], targets[i], speed * Time.deltaTime);

				if (p[i] != moveTo) {
					p[i] = moveTo;
					finished = false;
				}
			}

			yield return null;
		} while (finished == false);
	}

	Vector2[] CreatePointCircle (int numberOfPoints, float radius) {
		Vector2[] points = new Vector2[numberOfPoints];

		float degreesPerPoint = 360 / numberOfPoints;

		points[0] = new Vector2 (0, radius);

		for (int i = 1; i < points.Length; i++) {
			points[i] = Rotate(points[i - 1], degreesPerPoint);
		}

		return points;
	}

	Vector2[] DistortPoints (Vector2[] po, float strength) {
		Vector2[] p = new Vector2[po.Length]; //Create dereferenced new array to modify
		po.CopyTo(p, 0);
		//for (int a = 0; a < p.Length; a++) {
		//	p[a] = po[a];
		//}

		for (int i = 0; i < p.Length; i++) {
			float DistToPrevPoint = (i != 0 ? Vector2.Distance(p[i], p[i - 1]) : 0); //CHANGE THIS ----------------------------------------------------DONT USE DISTANCE
			float perlinValue = Mathf.PerlinNoise(seed + DistToPrevPoint * i, seed + 0);

			Vector2 randVector = Rotate(Vector2.up, perlinValue * 360f); //Choose random direction Vector2
			randVector *= perlinValue * distortStrength;
			p[i] += randVector;
		}

		return p;
	}
	
	Vector2 Rotate (Vector2 v, float degrees) {
		return Quaternion.Euler(0, 0, degrees) * v;
    }

	void OnDrawGizmos () {
		if (points != null) {
			for (int i=0; i < points.Length; i++) {
				if (points[i] != null) {
					Gizmos.color = Color.black;
					if (points[i + 1] == null) { //Last point in array
						Gizmos.color = Color.red;
					}
					Gizmos.DrawSphere(points[i], 0.1f);
				}
			}
		}
	}

	*/
}
