﻿using UnityEngine;
using System.Collections;

public class GameTime : Singleton<GameTime> {

	protected GameTime () {} //Cant use constructor to create GameTime class

	private float currentTime; //Current turn-based time in turns or 'seconds'

	public event System.EventHandler TimeAdvanced;
 
	void Start () {
		currentTime = 0;
	}
	
	void Update () {
		
	}

	public void AdvanceTime (float seconds) {
		currentTime += seconds;
		OnTimeAdvanced (System.EventArgs.Empty);
	}

	protected virtual void OnTimeAdvanced (System.EventArgs e) {
		if (TimeAdvanced != null) {
			TimeAdvanced (this, e);
		}
	}
}
