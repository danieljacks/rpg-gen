﻿using UnityEngine;
using System.Collections;
///Contains functions to handle grid-based and non grid-based movement
public class GameMovement : Singleton<GameMovement> {

	public int gridSize = 1; //Grid size in units

	public Vector2 AlignedToGrid (Vector2 pos) { ///Returns position needed if object at position pos is to be aligned with the grid
		Vector2 alignedPos;
		
		alignedPos.x = Mathf.Round(pos.x / gridSize) * gridSize; //Align to intersection of grid squares
		alignedPos.x += gridSize / 2; //Add half grid size to put character at center of grid square - accounts for the origin of spire being at the center of the sprite rather than at (0,0)

		alignedPos.y = Mathf.Round(pos.y / gridSize) * gridSize; //Align to intersection of grid squares
		alignedPos.y += gridSize / 2; //Add half grid size to put character at center of grid square - accounts for the origin of spire being at the center of the sprite rather than at (0,0)

		return alignedPos;
	}
}
