﻿using UnityEngine;
using System.Collections;

public abstract class UnitMovement : MonoBehaviour {

	//http://stackoverflow.com/questions/2158028/how-to-force-a-derived-class-to-include-certain-properties-with-default-value - Force derived calsses (units) to have a vector2 (real) position, which can be set to 0.5, say, and doesnt change where the unit appears (moving one tile can take 2 turns if each turn it moves 0.5 of a tile)
	protected abstract Vector2 posFraction {get; set;}
	protected const float moveAnimationSpeed = 1; //Base units per frame that the animation moves
	public const int gridSize = 1; //Grid size in units
	private float gridRadius;

	void Start () {
		gridRadius = gridSize / 2;
	}

	protected Vector2 AlignToGrid (Vector2 pos) { ///Returns position needed if object at position pos is to be aligned with the grid
		Vector2 alignedPos;
		
		alignedPos.x = Mathf.Round(pos.x / gridSize) * gridSize; //Align to intersection of grid squares
		alignedPos.x += gridSize / 2; //Add half grid size to put character at center of grid square - accounts for the origin of spire being at the center of the sprite rather than at (0,0)

		alignedPos.y = Mathf.Round(pos.y / gridSize) * gridSize; //Align to intersection of grid squares
		alignedPos.y += gridSize / 2; //Add half grid size to put character at center of grid square - accounts for the origin of spire being at the center of the sprite rather than at (0,0)

		return alignedPos;
	}

	protected bool TileIsEmpty (Vector2 pos) { //Tests whether there is anything in the specified tile
		Vector2 alignedPos = AlignToGrid (pos);

		Vector2 gridBoundsA = new Vector2 (alignedPos.x - gridRadius, alignedPos.y - gridRadius);
		Vector2 gridBoundsB = new Vector2 (alignedPos.x + gridRadius, alignedPos.y + gridRadius);

		Collider2D[] cols = new Collider2D[1];

		if ( Physics2D.OverlapAreaNonAlloc(gridBoundsA, gridBoundsB, cols, Physics.AllLayers, 0, 0) == 0) { //Nothing there
			return true;
		} else {
			return false;
		}
	}

	///Snaps original vector to grid, then moves it offset number of grid squares
	protected Vector2 moveAlongGrid (Vector2 original, Vector2 offset) { 
		Vector2 pos = AlignToGrid(original) + new Vector2 (Mathf.Round(offset.x), Mathf.Round(offset.y)) * gridSize;
		return pos;
	}

	protected float VectorDistance (Vector2 vectorA, Vector2 vectorB) {
		Vector2 currentRel = vectorB - vectorA;

		float currentDist;
		if (currentRel.x == 0) { 				//No movement along x axis
			if (currentRel.y != 0) { 			//	Movement along y axis
				currentDist = currentRel.y; 	//		Direction only in y axis
			} else {							//	No movement along y axis
				currentDist = 0; 				//		No movement
			}
		} else { 								//Movement along x axis
			if (currentRel.y == 0) { 			//	No movement along y axis
				currentDist = currentRel.x; 	//		Direction only in x axis
			} else {							//	Movement along y axis
				currentDist = 					//		Movement along both axis - find distance with pythagoras, higher processing
					Vector2.Distance
					(vectorA, vectorB);
			}
		}

		return currentDist;
	}
}