﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(PlayerBase))]
public class PlayerMove : UnitMovement {
	public float moveAnimNormMin = 0; 
	protected override Vector2 posFraction {get; set;}
	private Transform t;
	private PlayerBase playerBase; //Reference to script containing player info such as stats

	private bool moving = false;
	private bool lastDirHorizontal = false;
	private float horizontalPressedTime = 0;
	private float verticalPressedTime = 0;
	private bool horizontalPressed = false;
	private bool verticalPressed = false;
	private Vector2 moveDir = Vector2.zero;
	private Vector2 axisInput = Vector2.zero;
	private Vector2 queuedInput = Vector2.zero;
	private float moveAnimNorm = 0;
	void Start () {
		playerBase = GetComponent<PlayerBase>();

		t = transform;

		t.position = AlignToGrid(t.position);
	}
	
	// Update is called once per frame
	void Update () {
		SaveAxisPressedTimes();
		axisInput = CombineAxis( new Vector2 (Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")) );
		UseQueue();

		if (axisInput.x != 0 || axisInput.y != 0) { //Movement command sent and not currently moving
			if (!moving){
				StartMoving (axisInput, playerBase.stat.speed, playerBase.stat.animationSpeed);
				queuedInput = Vector2.zero; //Moved a square, no queued input
			} else {
				queuedInput = GetQueueAxis(axisInput, moveDir, moveAnimNorm, moveAnimNormMin);
			}
		}

		print ("moveDir: " + moveDir + " | queuedInput: " + queuedInput + " | moving: " + moving);
	}

	private void StartMoving (Vector2 direction, float speed, float animationSpeed) { //Direction for movement, where the value is a multiplier - 0.5 = half speed
		moveDir = direction;
		Vector2 playerPos = playerBase.transform.position;
		Vector2 target = moveAlongGrid(playerPos, direction); //Move target along with moveDir direction

		StartCoroutine(moveRoutine(target, animationSpeed)); //Start movement animation
	}

	private IEnumerator moveRoutine (Vector2 target, float animationSpeed) {
		moving = true;

		Vector2 moveStart = (Vector2)playerBase.transform.position;

		while ((Vector2)playerBase.transform.position != target) { //While player is not at the target, move towards the target
			Vector2 playerPos = (Vector2)playerBase.transform.position;
			playerPos = Vector2.MoveTowards(playerPos, target, moveAnimationSpeed * animationSpeed * Time.deltaTime);
			playerBase.transform.position = playerPos;

			moveAnimNorm = GetAnimNorm(moveStart, playerPos, target);

			print ("NORM: " + moveAnimNorm); //DEAL WITH THE ANIM NORM, TO mAKE MOVE COMMAND QUEUE ONLY WORK IN SEONDS HALF (OR WHATEVER VARIABLE IS SET TO) OF ANIMATION MAKE A QUEUE

			yield return null;
		}
		moving = false;
		moveDir = Vector2.zero;

		//Make animation speed cap at a point (tanh function)
		//Make walk animation loop again within itself if needed - no stutter between grid squares, check to see if all calculations are finished needed first...
	}

	private Vector2 CombineAxis (Vector2 axisInput) {
		Vector2 axisDir;

		if (axisInput.x != 0 && axisInput.y != 0) {
			if (horizontalPressedTime > verticalPressedTime) { //Horizontal axis pressed more recently
				axisDir = new Vector2 (axisInput.x, 0);
			} else {
				axisDir = new Vector2 (0, axisInput.y); //Vertical pressed more recently
			}
		} else { //Only one direction of movement input
			axisDir = new Vector2 (axisInput.x, axisInput.y);
		}

		return axisDir;
	}

	private float GetAnimNorm (Vector2 start, Vector2 current, Vector2 target) {
		float currentDist = VectorDistance(start, current);
		float totalDist = VectorDistance(start, target);

		float animNorm = currentDist / totalDist;

		return animNorm;
	}

	private Vector2 GetQueueAxis (Vector2 axisInput, Vector2 moveDir, float animNorm, float normMin) {
		if (animNorm < normMin) {
			return Vector2.zero; //Not far enough into animation to actually queue a move command
		}

		Vector2 queuedInput;

		if (moveDir != axisInput && moveDir != (-1 * axisInput) ) { //New direction is not in the direction of or directly opposite current direction of movement, do the move action at the next opportunity, if it is not cancelled
			queuedInput = axisInput;
		} else {
			queuedInput = Vector2.zero; //Cancel queued input if user presses same direction or opposite to movement
		}

		return queuedInput;
	}

	///Use queue if applicable, run in Update
	private void UseQueue () {
		if (queuedInput != Vector2.zero && !moving) {
			StartMoving (queuedInput, playerBase.stat.speed, playerBase.stat.animationSpeed); //Move with queued input
			queuedInput = Vector2.zero; //Used queue input
		}
	}

	private void SaveAxisPressedTimes () {
		if (Input.GetAxisRaw("Horizontal") != 0) {
			if (horizontalPressed == false) { //Key just pressed now
				horizontalPressedTime = Time.time; //Save press time
				horizontalPressed = true;
			}
		} else {
			horizontalPressed = false;
		}
		
		if (Input.GetAxisRaw("Vertical") != 0) {
			if (verticalPressed == false) { //Key just pressed now
				verticalPressedTime = Time.time; 
				verticalPressed = true;
			}
		} else {
			verticalPressed = false;
		}
	}

	void OnDrawGizmos () {
		//drawGizmoSphere();
	}

	void drawGizmoSphere (Vector2 pos, Color color) {
		Gizmos.color = color;
		Gizmos.DrawSphere(pos, 0.1f);
	}
}