﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerMove))]

public class PlayerBase : UnitBase {
	[SerializeField]
	public new Stats baseStat;
	[System.NonSerializedAttribute]
	public new Stats stat;

	void Awake () {
		stat = baseStat;
	}
	
	void Update () {
	
	}
}
