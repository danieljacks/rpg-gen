﻿using UnityEngine;
using System.Collections;

public abstract class UnitBase : MonoBehaviour {
	[System.SerializableAttribute]
	public class Stats {
		public float health; //Hitpoints
		public float damage; //How does it work?
		public float speed; //How fast unit attacks and moves, exact implementation to be figured out
		public float animationSpeed; //How many units the unit travels per second during their move animation
	}
	[System.NonSerializedAttribute]
	public Stats baseStat;
	[System.NonSerializedAttribute]
	public Stats stat;

	public float speedAsymptote = 0.2f; //Max speed that can be reached - i.e, 0.2 = 20% of default speed

	void Start () {
	
	}
	
	void Update () {
	
	}
}
