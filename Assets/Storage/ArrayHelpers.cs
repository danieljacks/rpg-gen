﻿using UnityEngine;
using System.Collections;

public static class ArrayHelpers {
	//public static int FlatIndex (this int[,] array2D, int ind1, int ind2) {
	//	return array2D.FlatIndex(ind1, ind2, true);
	//}

	/*/// <summary>
    /// Returns index of x, y position of an array as if that array were 1 dimensional.
	/// The 1d array's index flow on the 2d array from left to right, top to bottom.
    /// </summary>
    /// <param name="index">vector2 containing the first and seceond indexes of the 2d array</param>
    /// <param name="includeZero">whether or not the conceptual 1d array starts at 0 (if not it starts at 1)</param>
    /// <returns>index in the 1d array</returns>
	public static int FlatIndex (this int[,] array2D, Vector2 index, bool includeZero) {
		int x = (int) index.x;
		int y = (int) index.y;

		int width = array2D.GetLength (0);
		int i = x + y * width;

		if (includeZero == false) {
			i += 1; //Bump 1d array values up by one
		}

		return i;
	}

	/// <summary>
    /// Returns the x and y indices of the 1d array, if it were a representation of a 2d array.
    /// </summary>
    /// <param name="index">index in the 1d array to interpret</param>
    /// <param name="includeZero">whether the 1d array starts at 0 (if not it starts at 1)</param>
    /// <returns></returns>
	public static Vector2 GridIndex (this int [,] array1D, int index, int width, bool includeZero) {
		int i = index;
		int length = width;

		int x = i % length;
		int y = i / length;

		Vector2 indices = new Vector2 (x, y);
		return indices; 
	}*/

	public static int IndexGridToFlat (Vector2 indices, int width, bool includeZeroInFlat) {
		int x = (int) indices.x;
		int y = (int) indices.y;

		int i = x + y * width;

		if (includeZeroInFlat == false) {
			i += 1; //Bump 1d array values up by one
		}

		return i;
	}

	public static Vector2 IndexFlatToGrid (int index, int width, bool includeZeroInFlat) {
		if (includeZeroInFlat == false) {
			index -= 1;
		}

		int x = index % width;
		int y = index / width;

		Vector2 indices = new Vector2 (x, y);
		return indices; 
	}

	///Turn 2d array into 1d array
	public static byte[] Flatten (this byte[,] array2d) {
		int width = array2d.GetLength(0);
		int height = array2d.GetLength(1);

		byte[] array1d = new byte[width * height];

		for (int y = 0, i = 0; y < height; y++) {
			for (int x = 0; x < width; x++, i++) {
				array1d[i] = array2d[x, y];
			}
		}

		return array1d;
	}

	///Turn 1d array created with Flatten() into a 2d array with specified (original) dimensions
	public static byte[,] Expand (this byte[] array1d, int width, int height) {
		byte[,] array2d = new byte[width, height];

		if (array1d.Length != width * height) {
			string msg = string.Format ("1 dimensional array cannot be tranformed into a 2 dimensional array with width {0} and height {1}.", width, height);
			Debug.LogError(msg);
			return array2d; 
		}

		for (int y = 0, i = 0; y < height; y++) {
			for (int x = 0; x < width; x++, i++) {
				array2d[x, y] = array1d[i];
			}
		}

		return array2d;
	}

	//FOR COLOR ARRAYS
	public static Color32[] Flatten (this Color32[,] array2d) {
		int width = array2d.GetLength(0);
		int height = array2d.GetLength(1);

		Color32[] array1d = new Color32[width * height];

		for (int y = 0, i = 0; y < height; y++) {
			for (int x = 0; x < width; x++, i++) {
				array1d[i] = array2d[x, y];
			}
		}

		return array1d;
	}

	///Turn 1d array created with Flatten() into a 2d array with specified (original) dimensions
	public static Color32[,] Expand (this Color32[] array1d, int width, int height) {
		Color32[,] array2d = new Color32[width, height];

		if (array1d.Length != width * height) {
			string msg = string.Format ("1 dimensional array cannot be tranformed into a 2 dimensional array with width {0} and height {1}.", width, height);
			Debug.LogError(msg);
			return array2d; 
		}

		for (int y = 0, i = 0; y < height; y++) {
			for (int x = 0; x < width; x++, i++) {
				array2d[x, y] = array1d[i];
			}
		}

		return array2d;
	}

	///Turn 2d array into 1d array
	public static T[] Flatten<T> (this T[,] array2d) {
		int width = array2d.GetLength(0);
		int height = array2d.GetLength(1);

		T[] array1d = new T[width * height];

		for (int y = 0, i = 0; y < height; y++) {
			for (int x = 0; x < width; x++, i++) {
				array1d[i] = array2d[x, y];
			}
		}

		return array1d;
	}

	///Turn 1d array created with Flatten() into a 2d array with specified (i.e. original) dimensions
	public static T[,] Expand<T> (this T[] array1d, int width, int height) {
		T[,] array2d = new T[width, height];

		if (array1d.Length != width * height) {
			string msg = string.Format ("1 dimensional array cannot be tranformed into a 2 dimensional array with width {0} and height {1}.", width, height);
			Debug.LogError(msg);
			return array2d; 
		}

		for (int y = 0, i = 0; y < height; y++) {
			for (int x = 0; x < width; x++, i++) {
				array2d[x, y] = array1d[i];
			}
		}

		return array2d;
	}

}
