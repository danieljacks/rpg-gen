﻿using UnityEngine;
using System.Collections;

public class WorldData : Singleton<WorldData> {

	/*  Tile:           1 Byte              = 
        Chunk:          16x16               = (16x16)   256        Tiles                - 
        //Region:       16x16    = 256 chunks =     (256x256)    65,536     Tiles  = 0.065 Mb    - Added seperately to protobuff file, loaded individually to memory (4 at once, 0.2Mb)
        Sector:         256x256 = [256 Regions =]   (4096x4096)  16,777,216 Tiles  = 16.8 Mb     - Chunks individually collected from sector
        World:          2x2     = 4   Sectors =     (8,192)      67,108,864 Tiles  = 67.1 Mb     - Whole world
    */

    protected WorldData () {}
    
    public Region[,] regions;
}
