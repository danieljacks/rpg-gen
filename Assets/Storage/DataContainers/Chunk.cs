﻿using UnityEngine;
using System.Collections;
using ProtoBuf;

[ProtoContractAttribute]
public class Chunk : GridData<byte> {
	
	//Fields
	private const int _size = 16;

	//Properties
	protected override int Size { get { return _size; } }

	public static int SizeTiles { get { return _size; } }
	public static Chunk Empty { get { return CreateEmpty(); } }

	//Constructors
	public Chunk () : base () {}

	public Chunk (byte[] gridData) : base (gridData) { }

	//Methods
	private static Chunk CreateEmpty () {
		int s = SizeTiles * SizeTiles;

		byte[] empty = new byte[s];

		for (int i = 0; i < s; i++) {
			empty[i] = 0;
		}

		Chunk container = new Chunk (empty);

		return container;
	}

	//-------------------- Container-Specific Methods ---------------------------

	public Tile GetTile (int x, int y) {
		Tile tile = new Tile();

		byte key = GetDataVal(x, y);
		tile = Tile.GetTile(key);

		return tile;
	}

	public Texture2D GenerateTexture () {
		Texture2D tex = new Texture2D(_size, _size);
		Color32[] pixels = new Color32[_size * _size];

		for (int y = 0, i = 0; y < _size; y ++) {
			for (int x = 0; x < _size; x++, i++) {
				pixels[i] = GetTile(x, y).color;
			}
		}

		tex.SetPixels32(pixels);
		tex.Apply();

		return tex;
	}
}
