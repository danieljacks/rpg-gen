﻿using UnityEngine;
using System.Collections;
using ProtoBuf;
using System.IO;
using System.Text.RegularExpressions;
///Contains helper functions for handling world data
public static class World {
	//Fields
	//private Sector[,] _sectors;
	//private const int _sectorSize = 2;

	private const int _size = 2; //Real 4, set different for testing

	private const string sectorFilenameExpression = @"((?<x>\d+),(?<y>\d+))";

	//Has to be property for some error...
	private static string _sectorDirectory {
		get {
			return Application.persistentDataPath + @"/world/";
		}
	}

	//Properties
    ///Height/Width of world in tiles
	public static int SizeTiles {
		get {
			return Sector.SizeTiles * _size;
		}
	}
    ///Height/Width of world in chunks
	public static int SizeChunks {
		get {
			return Sector.SizeChunks * _size;
		}
	}

	///Height/Width of world in sectors
	public static int SizeSectors {
		get {
			return _size;
		}
	}

	public static string SectorDirectory {
		get {
			if (!Directory.Exists(_sectorDirectory)) {
				Directory.CreateDirectory(_sectorDirectory);
			}
			return _sectorDirectory;
		}
	}

	/// <summary>
    /// Returns the sector coordinates of a point on the map
    /// </summary>
    /// <param name="x">x position of DataType</param>
    /// <param name="y">y position of DataType</param>
    /// <param name="inputType">What grid of data x and y refer to</param>
	public static Vector2 GetSector (float x, float y, DataTypes inputType) {
		Vector2 sectorPos = new Vector2();

		switch (inputType) {
			case (DataTypes.Tile):
				int sectorSizeInTiles = Sector.SizeTiles;

				sectorPos.x = Mathf.Floor(x / sectorSizeInTiles); //* sectorSizeInTiles;
				sectorPos.y = Mathf.Floor(y / sectorSizeInTiles); //* sectorSizeInTiles;
				break;
			case (DataTypes.Chunk):
				int sectorSizeInChunks = Sector.SizeTiles / Chunk.SizeTiles;

				sectorPos.x = Mathf.Floor (x / sectorSizeInChunks); //* sectorSizeInChunks;
				sectorPos.y = Mathf.Floor (y / sectorSizeInChunks); //* sectorSizeInChunks;
				break;
			case (DataTypes.Sector):
				sectorPos.x = x;
				sectorPos.y = y;
				break;
			case (DataTypes.World):
				Debug.LogError("Cannot get the sector position of the entire world.");
				break;
			default:
				Debug.LogError("Unrecognised Datatype used for the inputType parameter.");
				break;
		}

		return sectorPos;
	}

	/// <summary>
    /// Returns the chunk coordinates of a point on the map
    /// </summary>
    /// <param name="x">X position of DataType</param>
    /// <param name="y">Y position of DataType</param>
    /// <param name="inputType">What grid of data x and y refer to</param>
	public static Vector2 GetChunk (float x, float y, DataTypes inputType) {
		Vector2 chunkPos = new Vector2();

		switch (inputType) {
			case (DataTypes.Tile):
				int chunkSizeInTiles = Chunk.SizeTiles;

				chunkPos.x = Mathf.Floor(x / chunkSizeInTiles) * chunkSizeInTiles;
				chunkPos.y = Mathf.Floor(y / chunkSizeInTiles) * chunkSizeInTiles;
				break;
			case (DataTypes.Chunk):
				chunkPos.x = x;
				chunkPos.y = y;
				break;
			case (DataTypes.Sector):
				Debug.LogError("Cannot get the chunk position of an entire sector.");
				break;
			case (DataTypes.World):
				Debug.LogError("Cannot get the chunk position of the entire world.");
				break;
			default:
				Debug.LogError("Unrecognised Datatype used for the inputType parameter.");
				break;
		}

		return chunkPos;
	}

	///Returns position within sector that chunk/tile occupies (sector coordinates)
	public static Vector2 SectorPos (float worldX, float worldY, DataTypes inputType) {
		Vector2 pos = new Vector2();

		switch (inputType) {
			case (DataTypes.Tile):
				int sectorSizeInTiles = Sector.SizeTiles;

				pos.x = worldX % sectorSizeInTiles;
				pos.y = worldY % sectorSizeInTiles;
				break;
			case (DataTypes.Chunk):
				int sectorSizeInChunks = Sector.SizeChunks;

				pos.x = worldX % sectorSizeInChunks;
				pos.y = worldY % sectorSizeInChunks;
				break;
			case (DataTypes.Sector):
				Debug.LogError("Cannot get relative sector position of an entire sector.");
				break;
			case (DataTypes.World):
				Debug.LogError("Cannot get the relative sector position of the entire world.");
				break;
			default:
				Debug.LogError("Unrecognised Datatype used for the inputType parameter.");
				break;
		}

		return pos;
	}

	public static void Save () {
		
	}

	public static void SaveChunk_OLD (Vector2 chunkPos, Chunk chunk) {
		Vector2 sectorPos 		= GetSector(chunkPos.x, chunkPos.y, DataTypes.Chunk);

		string sectorFilename 	= GenerateSectorFilename(sectorPos);
		
		//int chunkIndex = ArrayHelpers.IndexGridToFlat (chunkPos, Sector.WidthChunks, false);
		Vector2 chunkPosRelativeToSector = SectorPos(chunkPos.x, chunkPos.y, DataTypes.Chunk);
		int chunkIndex = ArrayHelpers.IndexGridToFlat (chunkPosRelativeToSector, Sector.SizeChunks, false);
		
		bool savedChunk = false;
		int i = 0; 
		using (FileStream fs = new FileStream(SectorDirectory + sectorFilename + ".sap", FileMode.OpenOrCreate, FileAccess.ReadWrite ) ) {
			while (fs.Position < fs.Length) {
				if (i == chunkIndex) {
					Serializer.SerializeWithLengthPrefix(fs, chunk, PrefixStyle.Base128);
					savedChunk = true;
					break;
				}
				
				Serializer.DeserializeWithLengthPrefix<Chunk>(fs, PrefixStyle.Base128); //Advance stream(?)
				i++;
			}

			if (savedChunk == false) {
				while (i <= chunkIndex) {
					if (i == chunkIndex) {
						Serializer.SerializeWithLengthPrefix(fs, chunk, PrefixStyle.Base128);
					} else {
						Serializer.SerializeWithLengthPrefix(fs, Chunk.Empty, PrefixStyle.Base128); //Fill until wanted save chunk with empty chunks
					}
					i++;
				}
			}
		}
	}

	public static Chunk LoadChunk_OLD (Vector2 chunkPos) {
		Vector2 sectorPos 		= GetSector(chunkPos.x, chunkPos.y, DataTypes.Chunk);
		string sectorFilename 	= GenerateSectorFilename(sectorPos);

		Vector2 chunkPosRelativeToSector = SectorPos(chunkPos.x, chunkPos.y, DataTypes.Chunk);
		int chunkIndex = ArrayHelpers.IndexGridToFlat (chunkPosRelativeToSector, Sector.SizeChunks, false);
		
		Chunk chunk = new Chunk();

		bool loadedChunk = false;
		int i = 0;
		using (FileStream fs = new FileStream(Application.persistentDataPath + @"/world/" + sectorFilename + ".sap", FileMode.OpenOrCreate, FileAccess.ReadWrite ) ) {
			while (fs.Position < fs.Length) {
				if (i == chunkIndex) {
					chunk = Serializer.DeserializeWithLengthPrefix<Chunk>(fs, PrefixStyle.Base128);
					loadedChunk = true;
					break;
				}
				
				Serializer.DeserializeWithLengthPrefix<Chunk>(fs, PrefixStyle.Base128); //Advance stream(?)
				i++;
			}

			if (loadedChunk == false) {
				Debug.Log("Reached end of sector file before finding chunk. Returning empty chunk.");
			}
		}

		return chunk;
	}

	public static void SaveChunk (Vector2 chunkPos, Chunk chunk) {
		string filename = ChunkFilename(chunkPos);
		string directory = ChunkDirectory (chunkPos);
		string extension = ".sap";

		using (FileStream fs = new FileStream (directory + filename + extension, FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
			Serializer.Serialize (fs, chunk);
			//Add move stream on
		}

		/*int chunkIndex = ArrayHelpers.IndexGridToFlat (chunkPosRelativeToSector, Sector.SizeChunks, false);
		
		bool savedChunk = false;
		int i = 0; 
		using (FileStream fs = new FileStream(SectorDirectory + sectorFilename + ".sap", FileMode.OpenOrCreate, FileAccess.ReadWrite ) ) {
			while (fs.Position < fs.Length) {
				if (i == chunkIndex) {
					Serializer.SerializeWithLengthPrefix(fs, chunk, PrefixStyle.Base128);
					savedChunk = true;
					break;
				}
				
				Serializer.DeserializeWithLengthPrefix<Chunk>(fs, PrefixStyle.Base128); //Advance stream(?)
				i++;
			}

			if (savedChunk == false) {
				while (i <= chunkIndex) {
					if (i == chunkIndex) {
						Serializer.SerializeWithLengthPrefix(fs, chunk, PrefixStyle.Base128);
					} else {
						Serializer.SerializeWithLengthPrefix(fs, Chunk.Empty, PrefixStyle.Base128); //Fill until wanted save chunk with empty chunks
					}
					i++;
				}
			}
		}*/
	}

	public static Chunk LoadChunk (Vector2 chunkPos) {
		string filename = ChunkFilename(chunkPos);
		string directory = ChunkDirectory (chunkPos);
		string extension = ".sap";

		Chunk chunk = new Chunk();

		using (FileStream fs = new FileStream (directory + filename + extension, FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
			chunk = Serializer.Deserialize<Chunk> (fs);
			//Add move stream on
		}

		return chunk;
	}

	public static void SaveTexture (Vector2 chunkPos, ProtoTexture texture) {
		string filename = ChunkFilename(chunkPos);
		string directory = ChunkDirectory (chunkPos);
		string extension = ".sap";

		using (FileStream fs = new FileStream (directory + filename + extension, FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
			Serializer.Serialize (fs, texture);
			//Add move stream on
		}

		/*int chunkIndex = ArrayHelpers.IndexGridToFlat (chunkPosRelativeToSector, Sector.SizeChunks, false);
		
		bool savedChunk = false;
		int i = 0; 
		using (FileStream fs = new FileStream(SectorDirectory + sectorFilename + ".sap", FileMode.OpenOrCreate, FileAccess.ReadWrite ) ) {
			while (fs.Position < fs.Length) {
				if (i == chunkIndex) {
					Serializer.SerializeWithLengthPrefix(fs, chunk, PrefixStyle.Base128);
					savedChunk = true;
					break;
				}
				
				Serializer.DeserializeWithLengthPrefix<Chunk>(fs, PrefixStyle.Base128); //Advance stream(?)
				i++;
			}

			if (savedChunk == false) {
				while (i <= chunkIndex) {
					if (i == chunkIndex) {
						Serializer.SerializeWithLengthPrefix(fs, chunk, PrefixStyle.Base128);
					} else {
						Serializer.SerializeWithLengthPrefix(fs, Chunk.Empty, PrefixStyle.Base128); //Fill until wanted save chunk with empty chunks
					}
					i++;
				}
			}
		}*/
	}

	public static ProtoTexture LoadTexture (Vector2 chunkPos) {
		string filename = ChunkFilename(chunkPos);
		string directory = ChunkDirectory (chunkPos);
		string extension = ".sap";

		ProtoTexture texture;

		using (FileStream fs = new FileStream (directory + filename + extension, FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
			texture = Serializer.Deserialize<ProtoTexture> (fs);
			//Add move stream on
		}

		return texture;
	}

	private static Vector2 ParseSectorFilename (string filename)  {
		Vector2 sectorPos = new Vector2();

		Match sectorFilenameMatch = Regex.Match(filename, sectorFilenameExpression);

		if (sectorFilenameMatch.Success) {
			int r;
			if (int.TryParse(sectorFilenameMatch.Groups['x'].Value, out r)) {
				sectorPos.x = r;
			} else {
				throw new SectorFilenameParseException("Sector filename " + filename + " countains an invalid x value.");
			}

			if (int.TryParse(sectorFilenameMatch.Groups['y'].Value, out r)) {
				sectorPos.y = r;
			} else {
				throw new SectorFilenameParseException("Sector filename " + filename + " countains an invalid y value.");
			}

		} else {
			throw new SectorFilenameParseException("Sector filename " + filename + " could not be parsed.");
		}

		return sectorPos;
	}

	private static string GenerateSectorFilename (Vector2 sectorPos) {
		string filename = "(" + sectorPos.x.ToString() + "," + sectorPos.y.ToString() + ")";
		
		return filename;
	} 

	///Generate directory and returns filename for given chunk
	private static string ChunkFilename (Vector2 chunkPos) {
		string filename = "(" + chunkPos.x.ToString() + "," + chunkPos.y.ToString() + ")";
		return filename;
	}

	private static string ChunkDirectory (Vector2 chunkPos) {
		Vector2 sectorPos = GetSector (chunkPos.x,  chunkPos.y, DataTypes.Chunk);
		string sectorDirectoryName = GenerateSectorFilename (sectorPos);

		string directory = _sectorDirectory + sectorDirectoryName;
		Directory.CreateDirectory (directory);

		return directory;
	} 
	
}

/// <summary>
/// Sector filename could not be parsed
/// </summary>
[System.Serializable]
public class SectorFilenameParseException : System.Exception {
	public SectorFilenameParseException() { }
	public SectorFilenameParseException( string message ) : base( message ) { }
	public SectorFilenameParseException( string message, System.Exception inner ) : base( message, inner ) { }
	protected SectorFilenameParseException(
		System.Runtime.Serialization.SerializationInfo info,
		System.Runtime.Serialization.StreamingContext context ) : base( info, context ) { }
}
