﻿/*using UnityEngine;
using System.Collections;

public class Sector_old : MonoBehaviour {

	//Fields
	private Region[,] _regions;
	//Regions in sector
	private const int _size = 4; //Real: 256, set lower fr testing...

	//Properties
	///Width of region in Tiles
	public static int Width { 
		get { return _size * Chunk.Width; } 
	}
	///Height of region in Tiles
	public static int Height { 
		get { return _size * Chunk.Height; } 
	}
	///Width of region in Chunks
	public static int WidthChunks { 
		get { return _size; } 
	}
	///Height of region in Chunks
	public static int HeightChunks { 
		get { return _size; } 
	}

	///Height/Width of Sector in tiles
	public static int SizeTiles {
		get { return _size * Chunk.Height; }
	}
	///Height/Width of Sector in Chunks
	public static int SizeChunks {
		get { return _size; }
	}

	//Methods
	public Region this [int x, int y] {
		get {
			if (x < 0 || x >= _size)
			{
				throw new System.ArgumentOutOfRangeException("Invalid x position");
			}
			if (y < 0 && y >= _size)
			{
				throw new System.ArgumentOutOfRangeException("Invalid y position");
			}
			return _regions[x, y];
		}
		set {
			if (x < 0 || x >= _size)
			{
				throw new System.ArgumentOutOfRangeException("Invalid x position");
			}
			if (y < 0 && y >= _size)
			{
				throw new System.ArgumentOutOfRangeException("Invalid y position");
			}
			_regions[x, y] = value;
		}
	}
}*/