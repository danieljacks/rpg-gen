﻿using UnityEngine;
using ProtoBuf;

[ProtoContract]
[ProtoIncludeAttribute(10, typeof(Tile))]
[ProtoIncludeAttribute(11, typeof(Chunk))]
[ProtoIncludeAttribute(12, typeof(Sector))]
[ProtoIncludeAttribute(13, typeof(TextureProto))]
///Base class for square, grid-based data. Generic parameter is the type of data container will hold.
public abstract class GridData<DataT> {
	[ProtoMemberAttribute(1)]
	protected DataT[] _data;
	
	//Properties
	private System.Type ContainerType { get { return GetType(); } } //Must be inehrited from GridData<>, used in place of GridData parent class

	protected abstract int Size { get; } //Size of data grid

	public virtual DataT this[int x, int y] {
		get {
			return GetDataVal (x, y);
		}
		set {
			SetDataVal (x, y, value);
		}
	}

	protected DataT GetDataVal (int x, int y) {
		Vector2 ind2d = new Vector2 (x, y);
		int ind1d = ArrayHelpers.IndexGridToFlat (ind2d, Size, true);
		return _data[ind1d];
	}

	protected void SetDataVal (int x, int y, DataT val) {
		Vector2 ind2d = new Vector2 (x, y);
		int ind1d = ArrayHelpers.IndexGridToFlat (ind2d, Size, true);
		_data[ind1d] = val;
	}

	//Constructors
	public GridData () {
		_data = new DataT[Size * Size];
	}

	public GridData (DataT[] gridData) {
		FillWithArray (gridData);
	}

	//Methods
	protected void FillWithArray (DataT[] gridData) {
		if (gridData.GetLength(0) == Size && gridData.GetLength(1) == Size) {
			DataT[,] clone = (DataT[,])gridData.Clone();
			_data = clone.Flatten<DataT>();
		} else {
			Debug.LogError("Given array does not match the dimensions of a chunk.");
		}
	}
}
