﻿using UnityEngine;
using System.Collections;

public class Sector : GridData<Chunk> {
	//Fields
	private const int _size = 4; //Real: 256, set lower for testing...

	//Properties
	protected override int Size { get { return _size; } }

	public static Sector Empty { get { return CreateEmpty(); } }

	//Constructors
	public Sector () : base () {}

	public Sector (Chunk[] gridData) : base (gridData) { }

	//Methods
	private static Sector CreateEmpty () {
		int s = SizeChunks * SizeChunks;

		Chunk[] empty = new Chunk[s];

		for (int i = 0; i < s; i++) {
			empty[i] = Chunk.Empty;
		}

		Sector container = new Sector (empty);

		return container;
	}

	public static int SizeTiles { get { return _size * Chunk.SizeTiles; } }
	public static int SizeChunks { get { return _size; } }
}
