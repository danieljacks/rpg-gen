﻿public class Region {
	//Fields
	private Chunk[,] _chunks;
	private const int _size = 16;

	//Properties
	///Width of region in Tiles
	public static int Width { 
		get { return _size * Chunk.SizeTiles; } 
	}
	///Height of region in Tiles
	public static int Height { 
		get { return _size * Chunk.SizeTiles; } 
	}

	//Methods
	public Chunk this [int x, int y] {
		get {
			if (x < 0 || x >= _size)
			{
				throw new System.ArgumentOutOfRangeException("Invalid x position");
			}
			if (y < 0 && y >= _size)
			{
				throw new System.ArgumentOutOfRangeException("Invalid y position");
			}
			return _chunks[x, y];
		}
		set {
			if (x < 0 || x >= _size)
			{
				throw new System.ArgumentOutOfRangeException("Invalid x position");
			}
			if (y < 0 && y >= _size)
			{
				throw new System.ArgumentOutOfRangeException("Invalid y position");
			}
			_chunks[x, y] = value;
		}
	}
}
