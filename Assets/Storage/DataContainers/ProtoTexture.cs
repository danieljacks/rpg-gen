﻿using UnityEngine;
using System.Collections;

[ProtoBuf.ProtoContractAttribute]
public class ProtoTexture : GridData<Color32> {
	
	//Fields
	private const int _size = 16;

	private const int _textureSize = 64;

	//Properties
	private float _textureScale { get { return _textureSize / _size; } }
	protected override int Size { get { return _size; } }

	public static int SizeTiles { get { return _size; } }
	public static ProtoTexture Empty { get { return CreateEmpty(); } }

	//Constructors
	public ProtoTexture () : base () {}

	public ProtoTexture (Color32[] gridData) : base (gridData) { }

	//Methods
	private static ProtoTexture CreateEmpty () {
		int s = SizeTiles * SizeTiles;

		Color32[] empty = new Color32[s];

		for (int i = 0; i < s; i++) {
			empty[i] = new Color32 (255, 255, 0, 255);
		}

		ProtoTexture container = new ProtoTexture (empty);

		return container;
	}

	//-------------------- Container-Specific Methods ---------------------------

	/*public Tile GetTile (int x, int y) {
		Tile tile = new Tile();

		byte key = GetDataVal(x, y);
		tile = Tile.GetTile(key);

		return tile;
	}*/

	public void Fill (Color32 color) {
		for (int y = 0; y < SizeTiles; y++) {
			for (int x = 0; x < SizeTiles; x++) {
				SetDataVal (x, y, color);
			}
		}
	}

	public Texture2D GenerateTexture () {
		float texPixelsPerTile = (float)_textureSize / (float)_size;
		float texPixelsPerTileMultiplier = 1 / texPixelsPerTile;

		Texture2D tex = new Texture2D(_textureSize, _textureSize);
		Color32[] pixels = new Color32[_textureSize * _textureSize];

		//Debug.Log ("val: " + GetDataVal ( Mathf.FloorToInt(9 * texPixelsPerTileMultiplier), Mathf.FloorToInt(9 * texPixelsPerTileMultiplier) );

		for (int y = 0, i = 0; y < _textureSize; y ++) {
			for (int x = 0; x < _textureSize; x++, i++) {
				pixels[i] = GetDataVal (Mathf.FloorToInt(x * texPixelsPerTileMultiplier), Mathf.FloorToInt(y * texPixelsPerTileMultiplier)); //Set texture size lower, debug to see if last x or last y value is out of array index exception
			}
		}

		tex.SetPixels32(pixels);
		tex.Apply();

		return tex;
	}
}