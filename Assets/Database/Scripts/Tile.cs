﻿using UnityEngine;

public class Tile {

	public string type {get; set;}

	public Color32 color {get; set;}

	///Creates and returns a tile of the type specified by tile id
	public static Tile GetTile (byte id) {
		Tile tile = new Tile ();

		if (! TileIndex.tiles.TryGetValue(id, out tile)) {
			Debug.LogError("Tile key does not exist in tile dictionary");
		}

		return tile;
	}
}
