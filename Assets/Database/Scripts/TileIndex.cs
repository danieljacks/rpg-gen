﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.IO;

public class TileIndex : Singleton<TileIndex> {
	private TileIndex () {}
	public static Dictionary<byte, Tile> tiles = new Dictionary<byte, Tile>();


	void Awake() {
		tiles = ReadXMLTiles();
	}

	Dictionary<byte, Tile> ReadXMLTiles () {
		Dictionary<byte, Tile> tiles = new Dictionary<byte, Tile>();

		FileStream stream = new FileStream(Path.Combine(Application.dataPath, @"Database/XML/Tiles.xml"), FileMode.Open, FileAccess.Read);

		XmlReader reader = XmlReader.Create(stream);

		Tile tile = new Tile();
		int id = -1;

		while (reader.MoveToNextAttribute() || reader.Read()) {
			if (id >= 255) {
				Debug.LogError("Tile id exceeds byte format size limitation. Too many tile types");
			}
			if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "tile") {
				if ( (tile.type != null) && (id >= 0) && !(tiles.ContainsKey((byte)id)) ) { //Something in prev tile, id set and not duplicate
					tiles.Add((byte)id, tile);
				}

				continue;
			}

			if (reader.NodeType == XmlNodeType.Attribute && reader.Name == "id") { 	//new tile, got id node
				tile = new Tile();

				int oi;										//Try getting an id from id node
				if (int.TryParse(reader.Value, out oi)) {
					id = oi;
				} else {
					id = -1;
				}

				continue;
			}

			if (reader.NodeType == XmlNodeType.Element) {
				switch(reader.Name) {
					case "type":
						tile.type = GetXmlElementString (reader.ReadSubtree());
						break;
					case "color":
						tile.color = GetXmlElementColor (reader.ReadSubtree(), "color");
						break;
				}

				continue;
			}
		}

		stream.Flush();
		stream.Close();

		return tiles;
	}

	int GetXmlElementInt (XmlReader pReader) {
		while (pReader.Read()) {
			if (pReader.NodeType == XmlNodeType.Text) {
				int intVal;
				if (int.TryParse(pReader.Value, out intVal)) { //Check if primitive value type
					return intVal;
				} else {
					Debug.LogError("Could not parse the xml value as an int");
					return -1;
				}
			}
		}

		Debug.LogError("Could not find any xml elements to parse.");
		return -1;
	}

	string GetXmlElementString (XmlReader pReader) {
		while (pReader.Read()) {
			if (pReader.NodeType == XmlNodeType.Text) {
				return pReader.Value;
			}
		}

		Debug.LogError("Could not find any xml elements to parse.");
		return "";
	}

	byte GetXmlElementByte (XmlReader pReader) {
		int val = GetXmlElementInt(pReader);

		val = Mathf.Clamp (val, 0, 255);

		return (byte)val;
	}

	Color32 GetXmlElementColor (XmlReader pReader, string endNodeName) {
		Color32 color = new Color32(0, 0, 0, 255);

		pReader.Read(); //Skip first element ('color' tag)

		while (pReader.Read()) {
			if (pReader.NodeType == XmlNodeType.EndElement && pReader.Name == endNodeName) { //End of color
				return color;
			}

			if (pReader.NodeType == XmlNodeType.Element) {
                switch (pReader.Name) {
                    case "r":
						color.r = GetXmlElementByte(pReader.ReadSubtree());
						break;
					case "g":
						color.g = GetXmlElementByte(pReader.ReadSubtree());
						break;
					case "b":
						color.b = GetXmlElementByte(pReader.ReadSubtree());
						break;
					case "a":
						color.a = GetXmlElementByte(pReader.ReadSubtree());
						break;
					default: //Unrecognised tag
						Debug.LogWarning("Unrecognised color tag in xml.");
						return color;
                }
            }
		}

		Debug.LogError("Could not find color end tag in xml.");
		return color;
	}
}
