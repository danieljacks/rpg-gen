﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(WorldViewer))]
public class WorldViewerEditor : Editor {
	private WorldViewer renderer;

	private void OnEnable () {
		renderer = target as WorldViewer;
		Undo.undoRedoPerformed += RefreshCreator;
	}

	private void OnDisable () {
		Undo.undoRedoPerformed -= RefreshCreator;
	}

	private void RefreshCreator () {
		if (Application.isPlaying) {
			WorldViewer.Instance.UpdateView ();
		}
	}

	public override void OnInspectorGUI () {
		DrawDefaultInspector();
		//Chaning fields in inspector doesnt trigger property auto-update, so this button is necessary
		if (GUILayout.Button("Update View")) {
			RefreshCreator();
		}
	}
}
