﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WGenTerrain))]
public class WGenTerrainEditor : Editor {

	private WGenTerrain renderer;

	private void OnEnable () {
		renderer = target as WGenTerrain;
		Undo.undoRedoPerformed += RefreshCreator;
	}

	private void OnDisable () {
		Undo.undoRedoPerformed -= RefreshCreator;
	}

	private void RefreshCreator () {
		if (Application.isPlaying) {
			WGenTerrain.Instance.Regenerate();
			TerrainRenderer.Instance.DrawTerrain();
		}
	}

	public override void OnInspectorGUI () {
		/*EditorGUI.BeginChangeCheck();
		DrawDefaultInspector();
		if (EditorGUI.EndChangeCheck()) {
			RefreshCreator();
		}*/

		DrawDefaultInspector();

		if (GUILayout.Button("Generate Terrain")) {
			RefreshCreator();
		}
	}
}
